#include <stdlib.h>
#include <pwd.h>
#include <fcntl.h>
#include <dirent.h>
#include <ctype.h>
#include "tlpi_hdr.h"
#include <sys/stat.h>

#define _GNU_SOURCE

#define UID_MAX 10
#define DIR_MAX 100
#define FILE_MAX 500

int userIdFromName(const char *, char **);
Boolean ranByUser(FILE *, char [], char [], char [], char []);

int main(int argc, char *argv[])
{
	if (argc == 1 || argc > 2 || strcmp(argv[1], "--help") == 0)
		usageErr("Usage: %s user_name", argv[0]);

	char uid[UID_MAX];
	char *p = uid; int u = userIdFromName(argv[1], &p);
	if (u == -1)
		fatal("No such user exists\n");
	printf("User: %s\n", uid);

	DIR *proc = opendir("/proc/");
	struct dirent *proc_dir;
	FILE *fp;
	char status_file[DIR_MAX];
	char name[FILE_MAX];
	char uid_file[FILE_MAX];
	char buf[FILE_MAX];

	while ((proc_dir = readdir(proc)) != NULL)
	{
		if (proc_dir->d_type != DT_DIR || 
				!isdigit( (unsigned char)proc_dir->d_name[0]))
			continue;

//		printf("%s\n", proc_dir->d_name);
		snprintf(status_file, DIR_MAX-1, "/proc/%s/status", proc_dir->d_name);

		fp = fopen(status_file, "r");
		if (fp == NULL)
			break;
		if (ranByUser(fp, uid, buf, name, uid_file))
			printf("%s\n", name);
	}
	exit(EXIT_SUCCESS);
}

int userIdFromName(const char *name, char **uid)
{
	struct passwd *pwd;
	uid_t u;
	char *endptr;

	if (name == NULL || *name == '\0')
		return -1;

	u = strtol(name, &endptr, 10);
	if (*endptr == '\0')
		return u;

	pwd = getpwnam(name);
	if (pwd == NULL)
		return -1;
	u = pwd->pw_uid;
	sprintf(*uid, "%d", u);

	return u;

}

Boolean ranByUser(FILE *fp, char uid_query[], char buf[], char name[], char uid_file[])
{
	int i = 0;
	buf[0] = name[0] = uid_file[0] = '\0';

	while(fgets(buf, FILE_MAX, fp) != NULL){
		if (strncmp(buf, "Name:", 5) == 0) {
			char *p = &buf[6];
			while (*p != '\n' && *p != '\0')
				name[i++] = *p++;
			name[i] = '\0';
			i = 0;
//			printf("%s\n", name);

		} else if (strncmp(buf, "Uid:", 4) == 0) {
			
			char *p = &buf[5];
			while(isdigit(*p))
				uid_file[i++] = *p++;
			uid_file[i] = '\0';
			i = 0;
			if (strcmp(uid_query, uid_file) == 0)
				return TRUE;
			else 
				return FALSE;
		}

	}

	return FALSE;
}
