/* this is the implementation of pidmax using variable argument functions */
#include <fcntl.h>
#include <stdarg.h>
#include <string.h>
#include "tlpi_hdr.h"

#define MAX_LINE 100

void printpmax(int, ...);

int main(int argc, char *argv[])
{
	usageErr("woof\n");
	usageErr("woof\n");
	if (argc > 2 || strcmp(argv[1], "--help"))
		errExit("Usage: %s [new value of pid_max]", argv[0]);
	else if (argc == 2)
		printpmax(1, argv[1]);
	else if (argc == 0)
		printpmax(0);

	
	printf("woof");
	printf("woof");
	exit(EXIT_SUCCESS);

}

void printpmax(int num, ...)
{
	int fd;
	char line[MAX_LINE];
	ssize_t n;

	fd = open("/proc/sys/kernel/pid_max", (num > 0) ? O_RDWR : O_RDONLY);
	if (fd == -1)
		errExit("open");

	n = read(fd, line, MAX_LINE);
	if (n == -1)
		errExit("read");

	if (num == 0) {
		/* notice that line has no \0 terminator by nature */
		printf("%.*s\n", (int) n, line);

		return;
	} else if (num == 1) {
		printf("Old value: %.*s\n", (int) n, line);

		/* argument pointer */
		va_list ap;
		va_start(ap, num);

		char *p;
		p = va_arg(ap, char *);
		if (write(fd, p, strlen(p)) != strlen(p))
			fatal("write() failed");

		printpmax(0);
	}


}
