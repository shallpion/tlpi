#ifndef GETPWNAM_H
#define GETPWNAM_H
#include <pwd.h>

struct passwd *getpwnam(const char *name);

#endif
