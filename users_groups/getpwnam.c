#include <sys/types.h>
#include <string.h>
#include "tlpi_hdr.h"
#include "getpwnam.h"


#define MAX_LEN 100

struct passwd *getpwnam(const char *name)
{
	static struct passwd pwnam;
	static char pwnam_name[MAX_LEN];
	static char pwnam_passwd[MAX_LEN];
	static char pwnam_gecos[MAX_LEN];
	static char pwnam_dir[MAX_LEN];
	static char pwnam_shell[MAX_LEN];
	struct passwd *pw;

	if (name == NULL || *name == '\0') {
		return NULL;
	}

	/* reset pwent from the beginning */
	setpwent();

	while ((pw = getpwent()) != NULL) {
		if (strcmp(pw->pw_name, name) == 0) {
			strncpy(pwnam_name, pw->pw_name, MAX_LEN);
			pwnam.pw_name = pwnam_name;
			strncpy(pwnam_passwd, pw->pw_passwd, MAX_LEN);
			pwnam.pw_passwd = pwnam_passwd;
			strncpy(pwnam_gecos, pw->pw_gecos, MAX_LEN);
			pwnam.pw_gecos = pwnam_gecos;
			strncpy(pwnam_dir, pw->pw_dir, MAX_LEN);
			pwnam.pw_dir = pwnam_dir;
			strncpy(pwnam_shell, pw->pw_shell, MAX_LEN);
			pwnam.pw_shell = pwnam_shell;
			return &pwnam;
		}
	}
	endpwent();
	return NULL;
}
