
/* this is my implementation of "tail [-n num] file". It firstly set file offset
 * to the end of file, then read backwards, each time filling in the buf[]
 * array, then we read the array to figure out how many \n is included until we
 * reach the start of the last n-th line, then we simply print out the rest of
 * the file  */

#include <fcntl.h>
#include <unistd.h>
#include <stdio.h>
#include "tlpi_hdr.h"

#define BUF_SIZE 64

int numNewLine(char *buf, int i, int size, char **p);
size_t back(int fd, ssize_t offset);

int main(int argc, char *argv[])
{
	/* default 10 lines output */
	int num = 10;
	int opt, fd, r;
	off_t offset;
	char buf[BUF_SIZE];
	char *p = NULL;


	while((opt = getopt(argc, argv, "n:")) != -1) {
		num = getLong(optarg, GN_ANY_BASE, "num");
	}


	fd = open(argv[argc-1], O_RDONLY);
	if (fd < 0)
		errExit("open");

	/* mark our position from EOF */
	offset = lseek(fd, -1, SEEK_END);

	/* File too small: */
	if (offset <= BUF_SIZE) {
		lseek(fd, 0, SEEK_SET);
		while((r=read(fd,buf,BUF_SIZE)) > 0)
			write(STDOUT_FILENO, buf, r);

		exit(EXIT_SUCCESS);
	}



	while (offset >= BUF_SIZE) {
		offset -= BUF_SIZE;

		/* move offset back by BUF_SIZE bytes */
		lseek(fd, -BUF_SIZE, SEEK_CUR);


		/* read, then move BUF_SIZE back again */
		r = read(fd, buf, BUF_SIZE);
		lseek(fd, -BUF_SIZE, SEEK_CUR);

		if (r != BUF_SIZE) {
			printf("r=%d\n", r);
			fatal("Error while reading the file");
		}

		/* count how many new lines we have found */
		num -= numNewLine(buf, num, BUF_SIZE, &p);

		/* we have found sufficient new lines */
		if (num <= 0) {
			/* set offset to the beginning of 10th lines, notice
			   the effect of +1	*/
			lseek(fd, (ssize_t)(p-buf+1), SEEK_CUR);
			while((r=read(fd, buf, BUF_SIZE)) > 0)
				write(STDOUT_FILENO, buf, r);
			break;
		} else
			continue;
	}


	/* requested too many lines, just dump everything */
	if (num > 0) {
		lseek(fd, 0, SEEK_SET);
		while((r=read(fd,buf,BUF_SIZE)) > 0)
			write(STDOUT_FILENO, buf, r);
	}
	exit(EXIT_SUCCESS);
}


int numNewLine(char *buf, int i, int size, char **p)
{
	int j = 0;
	char *pt;
	*p = buf+size-1;
	for (pt = buf+size-1; pt >= buf; pt--) {
		if (*pt != '\n')
			continue;
		else {
			j++;
			--i;
			if (i >= 0) 
				*p = pt;
		}
	}
	return j;

}

