#include<sys/stat.h>
#include <unistd.h>
#include<fcntl.h>
#include"../lib/tlpi_hdr.h"

#ifndef BUF_SIZE
#define BUF_SIZE 1024
#endif

int main(int argc, char *argv[])
{
	int opt, outputFd, openFlags;
	mode_t filePerms;
	ssize_t numRead;
	char buf[BUF_SIZE];

	filePerms = S_IRUSR | S_IWUSR | S_IRGRP | S_IWGRP | S_IROTH | S_IWOTH;		

	/* set open_flags */
	if (argc == 2){
		openFlags = O_CREAT | O_WRONLY | O_TRUNC;
		outputFd = open(argv[1], openFlags, filePerms);
	}
	else if (argc == 3) {
		while ((opt = getopt(argc, argv, "a")) != -1) {
			switch (opt) {
				case 'a':
					openFlags = O_CREAT | O_WRONLY | O_APPEND;
					outputFd = open(argv[2], openFlags, filePerms);
					break;
				default:
					usageErr("Usage: %s [-a] %s\n", argv[0], argv[2]);
			}
		}
	}

	/* read stdin and write to output file */
	while ((numRead = read(STDIN_FILENO, buf, BUF_SIZE)) > 0)
		if (write(outputFd, buf, numRead) != numRead)
			fatal("couldn't write whole buffer");
	if (numRead == -1)
		errExit("read");

	if (close(outputFd) == -1)
		errExit("close output");

	exit(EXIT_SUCCESS);
}

