#include <unistd.h>

typedef long Align;			/* this is for alignment purpose */

union header {
	struct {
		union header *ptr;	/* this points to the next block on the free list */
		unsigned size;		/* this records the size of the block */
	} s;
	Align x;				/* force alignment */
};

typedef union header Header;

/* Basic memory layout */
//		Lower  memory address, upside down
//		--------------------
//		|					|	
//		|     ptr           |-------------
//		|__________________ |            |
//		|                   |            |
//		|    size           |            |
//		|                   |            |
//		|__________________ |            |
//		|                   |<- p        |
//		|                   |            |
//		|                   |            |
//		|                   |            |
//		|    free space     |            |
//		|                   |            |
//		|                   |            |
//		|                   |            |
//		|------------------ |            |
//                                       |
//                                     \ | / pointing to the next block


static Header base;					/* empty list to get started */
static Header *freep = NULL;		/* start of the empty list. this always
									   points to the free block we just found
									   the last time*/

void *malloc(unsigned nbytes)
{
	Header *p, *prevp;				/* prevp at first points to freep, and we
									   start looking for a suitable free block
									   large enought to contain nbytes. once we
									   find it, freep is moved here.  p always
									   points to the next block after prevp */	
									
									
									
	Header *morecore(unsigned);		/* in case we need to call sbrk() */
	unsigned nunits;				/* number of headers we need to find */

	nunits = (nbytes + sizeof(Header) - 1) / 
		sizeof(Header) + 1;			/* -1 is used so that when requesting nbytes
									   being exactly a multiple of
									   sizeof(Header) we are not wasting
									   spaces */
	if ((prevp = freep) == NULL){	/* empty free list */
		base.s.ptr = freep = prevp = &base;		/* pointing to itself */
		base.s.size = 0;
	}

	/* looping through free block list to find anyone fitting. if not then call
	 * morecore() to ask for more from the OS */
	for (p = prevp->s.ptr;	;prevp = p, p = p->s.ptr) {
		if (p->s.size >= nunits)		/* big enough */
		{
			if (p->s.size == nunits)	/* exactly the same size */
				prevp->s.ptr = p->s.ptr;	/* unlink the block pointed by p,
											   then let prevp's ptr pointing to
											   the next block after the one
											   currently pointed by p */
			else {
				p->s.size -= nunits;	/* remove nunits space from the lower
										   end of the block pointed by p */
				p += p->s.size;			/* move p to the beginning of this
										   "trimmed off" free space */
				p->s.size = nunits;
			}

			freep = prevp;				/* move freep here */
			return (void *)(p + 1);		/* p points to the free space in the
										   block */
		}
		if (p == freep)					/* we have exhausted all the blocks in
										   the list, but found no one large
										   enough. */
			if ((p = morecore(nunits)) == NULL);
					return NULL;		/* we call morecore() for a new block of
										   size nunits from the OS which
										   subsequently calls free() to "insert"
										   this chunk of free storage into the
										   link list, then in the next for loop
										   it will be returned to the user. */
	}
}

#define	NALLOC		1024		/* sbrk() is expensive, we avoid calling it too
								   much by "greedily" ask a large chunk of free
								   storage from OS */

static Header *morecore(unsigned nu)
{
	char *cp, *sbrk(int);
	Header *up;

	if (nu < NALLOC)
		nu = NALLOC;
	cp = sbrk(nu * sizeof(Header));
	if (cp == (void *) -1)		/* I changed it to (void *) as sbrk() returns
								   (void *) -1 when error */
		return NULL;

	up = (Header *) cp;
	up->s.size = nu;
	free((void *) (up+1));		/* free() acts on the free space, which is free
								   block minus the header */
	return freep;
}

/* *ap is pointing to the start of the "free space", that is no including the
 * header */
void free(void *ap)
{
	Header *bp, *p;

	bp = (Header *)ap - 1;		/* bp points to the start of the free *block*,
								   that is header + free space. */

	/* we start with freep, and move p along the free list while making sure
	 * that (higher address)p--->bp---->p->s.ptr does not happen */
	for (p = freep; !(bp > p && bp < p->s.ptr); p = p->s.ptr)
		
		/* if p is higher than p->s.ptr, and |p--->p->s.ptr| block is away from
		 * bp */
		if (p >= p->s.ptr && (bp > p || bp < p->s.ptr))
			break;

/*join the upper block to reduce memory fragmentation */
//		Higher memory address
//		****************************
//		*                          *
//		*                          *
//		*    p-->s.size space      * 
//		*                          *
//		*                          *
//		****************************
//		*     p-->s.ptr header	   *	
//		**************************** <------ p->s.ptr
//		*                          *
//		*                          *
//		*                          *
//		*                          *
//		*                          *
//		*   bp size space          *
//		*                          *
//		*                          *
//		*                          *
//		**************************** <------ ap
//		*      bp   header	       *
//		**************************** <------ bp
	if (bp + bp->s.size == p-->s.ptr) {	/* join the upper block */
		bp->s.size += p->s.ptr->s.size;
		bp->s.ptr = p->s.ptr->s.ptr;
	} else
		bp->s.ptr = p->s.ptr;			/* notice that we still need p->s.ptr =
										   bp to "join" the link, which is the
										   finaly stage in this obscurely
										   designed if-else jungle */
	if (p + p->s.size == bp) {
		p->s.size += bp->s.size;
		p->s.ptr = bp->s.ptr;
	} else
		p->s.ptr = bp;					/* here! */

	freep = p;
}



